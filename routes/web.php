<?php

use App\Http\Controllers\About\AboutController;
use App\Http\Controllers\AdminController;

use App\Http\Controllers\HomeSliderControllelr;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::controller(AdminController::class)->group(function () {
    Route::get('/admin/logout', 'destroy')->name('admin.logout');
    Route::get('/admin/profile', 'profile')->name('admin.profile');
    Route::get('/edit/profile', 'EditProfile')->name('edit.profile');
    Route::post('/store/profile', 'StoreProfile')->name('store.profile');

    Route::get('/change/password', 'ChangePassword')->name('change.password');
    Route::post('/update/password', 'UpdatePassword')->name('update.password');

});
    Route::controller(HomeSliderControllelr::class)->group(function () {
    Route::get('/home/slide', 'HomeSlider')->name('home.slide');
    Route::post('/update/slider', 'UpdateSlider')->name('update.slider');
   });
Route::controller(AboutController::class)->group(function () {
    Route::get('/About/page', 'Aboutpage')->name('about.page');
    Route::post('/About/Update', 'UpdateAbout')->name('about.update');
    Route::get('/about', 'HomeAbout')->name('home.about');
    Route::get('/about/multi/image', 'AboutMultiImage')->name('about.multi.image');
    Route::post('/store/multi/image', 'StoreMultiImage')->name('store.multi.image');


});


Route::get('/dashboard', function () {
    return view('Admin.index');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';
